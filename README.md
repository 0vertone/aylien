# Aylien Proof of Concept

## Setup

**Install the following:**

* Ruby *2.6.4
* Node *Any
* Bundler

****
**Run**

Clone this repository.

Go to the rails directory

Run ```STUB_AYLIEN=true bin/bundle exec rails s```


Open your browser and navigate to http://localhost:3000/clusters


## Demo

A live demo lives here: https://aylien-shanemoore.herokuapp.com/clusters
