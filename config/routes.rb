Rails.application.routes.draw do
  root to: 'clusters#index'
  
  resources :clusters, only: [:index]
  resources :stories, only: [:index]
end
