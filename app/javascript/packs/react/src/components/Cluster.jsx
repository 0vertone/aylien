import React from 'react'
import PropTypes from 'prop-types';
import { Grid, IconButton, Card,  CardHeader, Avatar, CardActions, Icon } from '@material-ui/core';
import LinkIcon from '@material-ui/icons/Link';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import Flag from 'react-world-flags'

class Cluster extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { cluster } = this.props
        const readableDate = new Date(cluster.representative_story.published_at)
        return (
            <Card>
                <CardHeader
                    avatar={
                        <Avatar variant="square" aria-label="flag-avatar" subheader={cluster.location.country}>
                            <Icon fontSize="large">
                                <Flag code={cluster.location.country} />
                            </Icon>
                        </Avatar>
                    }
                    title={cluster.representative_story.title}
                    subheader={readableDate.toDateString()}
                />
                <CardActions disableSpacing>
                    <IconButton href={`/stories/?cluster_id=${cluster.id}`}>
                        <OpenInNewIcon />
                    </IconButton>
                    <IconButton href={cluster.representative_story.permalink}>
                        <LinkIcon />
                    </IconButton>
                </CardActions>
            </Card>
        )
    }
}

Cluster.propTypes = {
    cluster: PropTypes.shape({
        id: PropTypes.number,
        time: PropTypes.time,
        story_count: PropTypes.number,
        earliest_story: PropTypes.time,
        latest_story: PropTypes.time,
        location: PropTypes.shape({
            country: PropTypes.string
        }),
        representative_story: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            permalink: PropTypes.string,
            published_at: PropTypes.time
        })
    })
};

export default (Cluster)
