import React from 'react'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import {
    DateTimePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';

import DateFnsUtils from '@date-io/date-fns';

class RangeSelector extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            startTime: null,
            endTime: null
        }
    }


    render() {
        const {startTime, endTime} = this.state
        const callbackParent = () => { this.props.handleRangeSubmit(startTime, endTime) }

        return (
            <div>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container spacing={3} justify="space-around" direction="row" justify="center"
                        alignItems="center">
                        <Grid item xs>
                            <DateTimePicker value={startTime} label='Time Start' onChange={(value) => this.setState({ startTime: value }) }/>
                        </Grid>
                        <Grid item xs>
                            <DateTimePicker value={endTime} label='Time End' onChange={(value) => this.setState({ endTime: value }) }/>
                        </Grid>
                        <Grid item xs>
                            <Button variant="contained" onClick={callbackParent}>Search</Button>
                        </Grid>
                    </Grid>
                </MuiPickersUtilsProvider>

            </div>
        )
    }
}

RangeSelector.propTypes = {
    handleRangeSubmit: PropTypes.func,
};

export default (RangeSelector)
