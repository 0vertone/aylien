import React from 'react'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card, CardHeader, CardMedia, CardContent, Collapse, Paper, Chip } from '@material-ui/core';

class Story extends React.Component {
  constructor(props) {
      super(props)     
  }

  renderCategories() {
    const { categories } = this.props.story
    return(
      <Paper>
        {categories.map(category => {
          let color = category.confident ? 'primary' : 'secondary'
          return (
            <Chip
              color={color}
              key={category.id}
              label={`${category.id}: ${category.score}`}
            />
          )
        })}
      </Paper>
    )
  }

  renderHashTags() {
    const { hashtags } = this.props.story

    return(
      <Paper>
       {hashtags.map(hashtag => {
        return (
          <Chip
            key={hashtag}
            label={hashtag}
          />
        );
      })}
      </Paper>
    )
  }

  render() {
    const { story } = this.props
    const readableDate = new Date(story.published_at)
    const representedMedia = story.media[0]
    const mediaUrl = representedMedia && representedMedia.url
    const mediaHeight = representedMedia && representedMedia.height


    return (
      <Card>
        <CardHeader
          title={story.title}
          subheader={readableDate.toDateString()}
        />
        <CardMedia
          style={{ height: mediaHeight }}
          image={mediaUrl}
        />
        <CardContent>
          {this.renderHashTags()}
        </CardContent>
        <CardContent>
          {this.renderCategories()}
        </CardContent>
        {/* TODO: Make the content collapsible */}
        <Collapse in timeout="auto" unmountOnExit>
          <CardContent>
            <Typography gutterBottom>
              {story.body}
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
    )
  }
}



Story.propTypes = {
  story: PropTypes.shape({
    id: PropTypes.number,
    body: PropTypes.string,
    categories: PropTypes.arrayOf(PropTypes.shape({
      confident: PropTypes.bool,
      id: PropTypes.string,
      level: PropTypes.number,
      links: PropTypes.shape({self: PropTypes.string}),
      score: PropTypes.number,
      taxonomy: PropTypes.string
    })),
    entities: PropTypes.shape({}),
    keywords: PropTypes.arrayOf(String),
    language: PropTypes.string,
    title: PropTypes.string,
    published_at: PropTypes.string,
    media: PropTypes.arrayOf(
      PropTypes.shape({
        content_length: PropTypes.number,
        format: PropTypes.string,
        height: PropTypes.number,
        type: PropTypes.string,
        url: PropTypes.string,
        width: PropTypes.number,
      })
    ),
    hashtags: PropTypes.arrayOf(String)
  })
};

export default (Story)
