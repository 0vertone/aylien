import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import { MuiThemeProvider, ThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import green from '@material-ui/core/colors/green'

const theme = createMuiTheme({
  palette: {
    primary: green
  }
})

import Stories from './pages/Stories'
import Clusters from './pages/Clusters'
import Header from './pages/Header'

import './App.css'

class App extends Component {
  render () {

    return (
      <MuiThemeProvider theme={theme}>        
        <div className='main'>
          <Route path='/' component={Header} />
          <Route exact path='/clusters' component={Clusters} />
          <Route path='/stories' component={Stories} />
        </div>
      </MuiThemeProvider>
    )
  }
}

export default (App)
