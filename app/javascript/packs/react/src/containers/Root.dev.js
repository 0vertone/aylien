import React from 'react'
import { Route } from 'react-router-dom'
import {hot} from 'react-hot-loader'
import App from './App'

const Root = ({ store }) => (
  <div>
    <Route path='/' component={App} />
  </div>
)

export default hot(module)(Root)
