import React from 'react'
import { Route } from 'react-router-dom'
import App from './App'

const Root = ({ store }) => (
  <div>
    <Route path='/' component={App} />
  </div>
)

export default Root
