import React, { Component, useState } from 'react'
import { Typography, Grow, Fade } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import axios from 'axios'
import RangeSelector from '../../components/RangeSelector';

import Cluster from '../../components/Cluster';

class Clusters extends Component {

    constructor(props){
        super(props)

        this.state = {
            clusters: []
        };

        this.handleRangeSubmit = this.handleRangeSubmit.bind(this);
    }

    
    getHttpClusters(startTime, endTime) {
        const params = {
            'time.start': startTime,
            'time.end':  endTime,
            per_page: 32, // 4 columns * 8 row card in layout. TODO Possibly make this configurable?
            sort_by: 'latest_story', // TODO make this configurable
            cursor: null // TODO Store this in state as a var
        }
        
        axios.get('/clusters.json', { 
            headers: {'Content-Type': 'application/json'},
            params
        })
        .then((response) => {
            this.setState({
                clusters: response.data.clusters,
                nextPageCursor: response.data.next_page_cursor,
                clusterCount: response.data.cluster_count
            });
        });
    }
    

    componentDidMount() {
        this.getHttpClusters()
    }

    handleRangeSubmit(start, end) {
        this.getHttpClusters(start, end)
    }

    renderClusters(){
        const {clusters} = this.state
        return clusters.map((cluster) => 
            <Grid item key={cluster.id} xs={6} sm={4} md={3}>
                <Cluster cluster={cluster} />
            </Grid>
            )
    }

    render() {
        return (
            <Grid justify='center' alignContent="center" alignItems="center" container spacing={4} >
                <Grid item xs={12}>
                    <Typography>
                        Clusters
                    </Typography>
                </Grid>
                <Grid item>
                    <RangeSelector handleRangeSubmit={this.handleRangeSubmit}/>
                </Grid>
                <Grid item xs={12}> 
                    <Fade in timeout={1000}>
                        <Grid container justify='center' alignContent="center" alignItems="center">
                            {this.renderClusters()}
                        </Grid>
                    </Fade>
                </Grid>
            </Grid>
        )
    }
}

export default (Clusters)
