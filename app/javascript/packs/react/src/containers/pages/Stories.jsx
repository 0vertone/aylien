import React, { Component } from 'react'
import { Grid, Typography, Grow, Fade } from '@material-ui/core'
import PropTypes from 'prop-types';
import axios from 'axios'
import * as qs from 'query-string';
import Story from '../../components/Story.jsx'

class Stories extends Component {
    constructor (props) {
      super(props)

      this.state = {
        stories: []
      };
    }


    componentDidMount() {
      this.getHttpStories()
    }

    getHttpStories() {
      const query = qs.parse(location.search)
      const params = {
        cluster_ids: [query.cluster_id] // TODO: Update this so that it accepts multiple cluster ids
      }

      console.log(params);
      
      axios.get('/stories.json', { 
          headers: {'Content-Type': 'application/json'},
          params
      })
      .then((response) => {
        console.log(response.data.stories)
        this.setState({
            stories: response.data.stories
        });
      });
  }

  renderStories(){
    const {stories} = this.state
    return stories.map((story) => 
        <Grid item key={story.id} xs={12} sm={6} >
            <Fade in timeout={1000}>
              <Story story={story} />
            </Fade>
        </Grid>
        )
}
  
    render () {
      return (
        <Grid justify='center' alignContent="center" alignItems="center" container spacing={4} >
           {this.renderStories()}
        </Grid>
      )
    }
  }
  

Stories.propTypes = {
    cluster_id: PropTypes.number
};

  export default (Stories)
  