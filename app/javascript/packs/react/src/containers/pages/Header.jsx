import React, { Component, useState } from 'react'
import { Grow, Typography } from '@material-ui/core'
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid'

class Header extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Grid justify='center' alignContent="center" alignItems="center" container spacing={4} >
        <Grid item xs={12}>
          <Typography>
            Aylien
          </Typography>
        </Grid>
      </Grid>
    )
  }
}

export default (Header)
