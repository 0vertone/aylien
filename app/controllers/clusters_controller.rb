# frozen_string_literal: true

class ClustersController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.json { render json: index_json }
    end
  end

  private

  def index_json
    if ENV['STUB_AYLIEN']
      clusters_file_path = Rails.root.join('lib', 'files', 'clusters.json')
      return JSON.parse IO.read(clusters_file_path)
    end

    begin
      result = aylien_api_instance.list_clusters(aylien_opts(index_params))
      @json = result
    rescue AylienNewsApi::ApiError => e
      Rails.logger.error("Exception when calling DefaultApi->list_clusters :: #{e}")
      @json = common_error_json(http_code: 500, message: 'Something broke when contacting Aylien API')
    rescue StandardError => e
      Rails.logger.error("Exception in def index for #{self.class} :: #{e}")
      @json = common_error_json(http_code: 500, message: 'Something broke internally')
    end

    @json
  end

  def index_params
    params.permit('time.start', 'time.end', :per_page, :sort_by, :format)
  end

  def aylien_opts(params)
    time_start = Time.parse(params.fetch('time.start', Time.at(0).to_s))
    time_end = Time.parse(params.fetch('time.end', Time.now.utc.to_s))
    {
      'time_start' => time_start.iso8601,
      'time_end' => time_end.iso8601
    }
  end
end
