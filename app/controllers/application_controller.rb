# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def common_error_json(http_code:, message: 'An error occurred')
    { message: message, status: http_code }
  end

  def aylien_api_instance
    @aylien_api ||= begin
      AylienNewsApi::DefaultApi.new
    end
  end
end
