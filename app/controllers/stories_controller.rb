# frozen_string_literal: true

class StoriesController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.json { render json: index_json }
    end
  end

  private

  def index_json
    if ENV['STUB_AYLIEN']
      stories_file_path = Rails.root.join('lib', 'files', 'stories.json')
      return JSON.parse IO.read(stories_file_path)
    end

    begin
      result = aylien_api_instance.list_stories(aylien_opts)
      @json = result
    rescue AylienNewsApi::ApiError => e
      Rails.logger.error("Exception when calling DefaultApi->list_clusters :: #{e}")
      @json = common_error_json(http_code: 500, message: 'Something broke when contacting Aylien API')
    rescue StandardError => e
      Rails.logger.error("Exception in def index for #{self.class} :: #{e}")
      @json = common_error_json(http_code: 500, message: 'Something broke internally')
    end

    @json
  end

  def index_params
    params.require(:cluster_ids)
  end

  def aylien_opts
    {
      clusters: index_params
    }
  end
end
