FROM ruby:2.6.4
LABEL maintainer Shane <shanethmoore@gmail.com>

RUN gem install bundler

RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs 4

COPY . .

EXPOSE 3000

CMD ["bundle exec rails s"]
